#include <stdio.h>
#include <sys/types.h>
#include <dirent.h>

void print_content_dir(DIR *folder){
	struct dirent *de;
	while (de = readdir(folder)){
		printf("%s\n", de->d_name);
	}
}

int main(int argc, char *argv[]){
	DIR *current_dir;
	DIR *argv_dir;
	if (((current_dir = opendir(".")) == NULL) || ((argv_dir = opendir(argv[1]))== NULL)){
		printf("Ошибка в открытии каталога\n");
		return -1;
	}
	printf("Текущая директория:\n");
	print_content_dir(current_dir);
	printf("Директория %s:\n", argv[1]);
	print_content_dir(argv_dir);
	int current_dir_close = closedir(current_dir);
	int argv_dir_close = closedir(argv_dir);
	if (current_dir_close != 0 || argv_dir_close != 0){
		printf("Ошибка в закрытии каталога\n");
		return -1;
	}
	return 0;
}
