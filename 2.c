#include <stdio.h>

int main(int argc, char *argv[]){
	FILE *fd = fopen(argv[1], "w");
	int input;
	char file_wr;
	if (fd == NULL){
		printf("Ошибка в открытии файла\n");
		return -1;
	}
	while (1) {
		input = getc(stdin);
		if (input == 6){
			break;
		}
		file_wr = fputc(input, fd);
		if (!file_wr){
			printf("Символ '%c' не может быть записан\n", file_wr);
		}
	}
	if (fclose(fd) != 0){
		printf("Ошибка в закрытии файла\n");
	}
	return 0;
}
