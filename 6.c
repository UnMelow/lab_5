#include <dirent.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>

struct stat_list {
    long size;
    char fpath[4096];
    struct stat_list *next;
} *stat_files, *stat_head;

struct dup_list {
    char fpath1[4096];
    char fpath2[4096];
    struct dup_list *next;
} *dup_files, *dups_head;

int compare_files(FILE *f1, FILE *f2){
    char ch1 = getc(f1);
    char ch2 = getc(f2);
    while (ch1 != EOF && ch2 != EOF){
        if (ch1 != ch2){
          return 0;
        }
        ch1 = getc(f1);
        ch2 = getc(f2);
    }
    return 1;
}

int check_files(char *p2, long size)
{
    stat_files = stat_head;
    while(stat_files != NULL) {
        if (stat_files->size == size) {
            FILE *f1 = fopen(stat_files->fpath, "r");
            FILE *f2 = fopen(p2, "r");
            if (compare_files(f1, f2)) {
                struct dup_list *dup_elem = (struct dup_list *)malloc(sizeof(struct dup_list));
                dup_elem->next = NULL;
                snprintf(dup_elem->fpath1, 4096, "%s", stat_files->fpath);
                snprintf(dup_elem->fpath2, 4096, "%s", p2);
                if (dups_head == NULL) {
                    dups_head = dup_elem;
                    dup_files = dups_head;
                }
                else {
                    dup_files->next = dup_elem;
                    dup_files = dup_elem;
                }
            }
            fclose(f1);
            fclose(f2);
        }
        stat_files = stat_files->next;
    }
}


void scan_folder(char *path, int save_data)
{
    DIR *d = opendir(path);
    if (d == NULL) {
        return;
    }

    struct dirent *dir;
    struct stat st;
    while ((dir = readdir(d)) != NULL) {
        char d_path[4096];
        snprintf(d_path, 4096, "%s/%s", path, dir->d_name);
        if(dir-> d_type != DT_DIR) {
         
            stat(d_path, &st);
            if (save_data == 1) {
                struct stat_list *st_elem = (struct stat_list *)malloc(sizeof(struct stat_list));
                st_elem->size = st.st_size;
                st_elem->next = NULL;
                snprintf(st_elem->fpath, 4096, "%s", d_path);
                if (stat_head == NULL) {
                    stat_head = st_elem;
                    stat_files = stat_head;
                }
                else {
                    stat_files->next = st_elem;
                    stat_files = st_elem;
                }
            }
            else {
                check_files(d_path, st.st_size);
            }
        }
        else if(dir->d_type == DT_DIR && strcmp(dir->d_name, ".") != 0 && strcmp(dir->d_name, "..") != 0 )
        {
            scan_folder(d_path, save_data); 
        }
    }
    closedir(d);
}

int main(int argc, char **argv)
{
    stat_files = NULL;
    stat_head = NULL;
    dup_files = NULL;
    dups_head = NULL;
    scan_folder(argv[1], 1);
    scan_folder(argv[2], 0);
    FILE *fres = fopen(argv[3], "w");

    dup_files = dups_head;
    while(dup_files != NULL) {
        struct stat st1, st2;
        stat(dup_files->fpath1, &st1);
        stat(dup_files->fpath2, &st2);
        printf("%s (size: %ld, created: %ld, perms: %u, inode: %lu) duplicates :\n    %s (size: %ld, created: %ld, perms: %u, inode: %lu)\n",
                        dup_files->fpath1, st1.st_size, st1.st_ctime, st1.st_mode, st1.st_ino, dup_files->fpath2,
                        st2.st_size, st2.st_ctime, st2.st_mode, st2.st_ino );
        fprintf(fres, "%s (size: %ld, created: %ld, perms: %u, inode: %lu) duplicates :\n    %s (size: %ld, created: %ld, perms: %u, inode: %lu)\n",
                        dup_files->fpath1, st1.st_size, st1.st_ctime, st1.st_mode, st1.st_ino, dup_files->fpath2,
                        st2.st_size, st2.st_ctime, st2.st_mode, st2.st_ino );
        dup_files = dup_files->next;
    }
    fclose(fres);

    stat_files = stat_head;
    while(stat_files != NULL) {
        struct stat_list *st_elem = stat_files->next;
        free(stat_files);
        stat_files = st_elem;
    }

    dup_files = dups_head;
    while(dup_files != NULL) {
        struct dup_list *dup_elem = dup_files->next;
        free(dup_files);
        dup_files = dup_elem;
    }

    return 0;
}
